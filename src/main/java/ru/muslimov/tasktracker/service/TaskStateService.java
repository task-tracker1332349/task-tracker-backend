package ru.muslimov.tasktracker.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.muslimov.tasktracker.dto.TaskStateDto;
import ru.muslimov.tasktracker.exceptions.models.BadRequestException;
import ru.muslimov.tasktracker.exceptions.models.NotFoundException;
import ru.muslimov.tasktracker.mapper.TaskStateMapper;
import ru.muslimov.tasktracker.models.Project;
import ru.muslimov.tasktracker.models.TaskState;
import ru.muslimov.tasktracker.repository.TaskStateRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskStateService {

    private final TaskStateRepository taskStateRepository;
    private final TaskStateMapper taskStateMapper;
    private final ProjectService projectService;

    @Transactional
    public TaskStateDto createTaskState(Long projectId, String taskStateName) {

        if (taskStateName.isBlank()) {
            throw new RuntimeException("Task state name can't be empty.");
        }

        Project project = projectService.getProjectOrThrowException(projectId);

        project
                .getTaskStates()
                .stream()
                .map(TaskState::getName)
                .filter(anotherTaskStateName -> anotherTaskStateName.equalsIgnoreCase(taskStateName))
                .findAny()
                .ifPresent(it -> {
                    throw new BadRequestException(String.format("Task state %s already exists.", taskStateName));
                });

        TaskState taskState = taskStateRepository.saveAndFlush(
                TaskState
                        .builder()
                        .name(taskStateName)
                        .project(project)
                        .build()
        );

        return taskStateMapper.toDto(taskState);
    }

    @Transactional
    public List<TaskStateDto> getTaskStates(Long projectId) {

        Project project = projectService.getProjectOrThrowException(projectId);

        return project
                .getTaskStates()
                .stream()
                .map(taskStateMapper::toDto)
                .toList();
    }

    @Transactional
    public TaskStateDto getTaskState(Long taskStateId) {


        TaskState taskState = getTaskStateOrThrowException(taskStateId);

        return taskStateMapper.toDto(taskState);
    }

    @Transactional
    public TaskStateDto updateTaskState(Long taskStateId, String taskStateName) {

        if (taskStateName.isBlank()) {
            throw new BadRequestException("Task state name can't be empty.");
        }

        TaskState taskState = getTaskStateOrThrowException(taskStateId);

        taskState.setName(taskStateName);

        taskStateRepository.saveAndFlush(taskState);

        return taskStateMapper.toDto(taskState);
    }

    @Transactional
    public Boolean deleteTaskState(Long taskStateId) {

        getTaskStateOrThrowException(taskStateId);

        taskStateRepository.deleteById(taskStateId);

        return true;
    }

    protected TaskState getTaskStateOrThrowException(Long taskStateId) {
        return taskStateRepository.findById(taskStateId)
                .orElseThrow(() ->
                        new NotFoundException(String.format("Task state with id:%s doesn't exist.", taskStateId)));
    }

}
