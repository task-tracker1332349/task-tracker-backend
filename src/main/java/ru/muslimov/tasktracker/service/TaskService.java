package ru.muslimov.tasktracker.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.muslimov.tasktracker.dto.TaskDto;
import ru.muslimov.tasktracker.exceptions.models.BadRequestException;
import ru.muslimov.tasktracker.exceptions.models.NotFoundException;
import ru.muslimov.tasktracker.mapper.TaskMapper;
import ru.muslimov.tasktracker.models.Task;
import ru.muslimov.tasktracker.models.TaskState;
import ru.muslimov.tasktracker.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final TaskStateService taskStateService;

    @Transactional
    public TaskDto createTask(Long taskStateId, String taskName) {

        if (taskName.isBlank()) {
            throw new BadRequestException("Task state name can't be empty.");
        }

        TaskState taskState = taskStateService.getTaskStateOrThrowException(taskStateId);

        taskState
                .getTasks()
                .stream()
                .map(Task::getName)
                .filter(anotherTask -> anotherTask.equalsIgnoreCase(taskName))
                .findAny()
                .ifPresent(it -> {
                    throw new BadRequestException("Task name can't be empty.");
                });

        Task task = taskRepository.saveAndFlush(
                Task
                        .builder()
                        .name(taskName)
                        .taskState(taskState)
                        .build()
        );

        return taskMapper.toDto(task);
    }

    @Transactional
    public List<TaskDto> getTasks(Long taskStateId) {

        TaskState taskState = taskStateService.getTaskStateOrThrowException(taskStateId);

        return taskState
                .getTasks()
                .stream()
                .map(taskMapper::toDto)
                .toList();
    }

    @Transactional
    public TaskDto getTask(Long taskId) {

        Task task = getTaskOrThrowException(taskId);

        return taskMapper.toDto(task);
    }


    @Transactional
    public TaskDto editTask(Long taskId, Optional<String> taskName, Optional<String> taskDescription) {

        taskName = taskName.filter(name -> !name.isBlank());
        taskDescription = taskDescription.filter(description -> !description.isBlank());

        Task updatedTask = getTaskOrThrowException(taskId);

        taskName.ifPresent(updatedTask::setName);
        taskDescription.ifPresent(updatedTask::setDescription);

        return taskMapper.toDto(updatedTask);

    }

    @Transactional
    public Boolean deleteTask(Long taskId) {

        getTaskOrThrowException(taskId);

        taskRepository.deleteById(taskId);

        return true;
    }

    protected Task getTaskOrThrowException(Long taskId) {
        return taskRepository.findById(taskId)
                .orElseThrow(() ->
                        new NotFoundException(String.format("Task with id:%s doesn't exist.", taskId)));
    }
}
