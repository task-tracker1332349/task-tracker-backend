package ru.muslimov.tasktracker.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.muslimov.tasktracker.dto.CreationUserDto;
import ru.muslimov.tasktracker.dto.UserDto;
import ru.muslimov.tasktracker.mapper.UserMapper;
import ru.muslimov.tasktracker.models.Role;
import ru.muslimov.tasktracker.models.User;
import ru.muslimov.tasktracker.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public UserDto createUser(CreationUserDto newUser) {

        User user = userRepository.saveAndFlush(
                User
                        .builder()
                        .name(newUser.getUsername())
                        .email(newUser.getEmail())
                        .password(passwordEncoder.encode(newUser.getPassword()))
                        .role(Role.USER)
                        .build()
        );

        return userMapper.toDto(user);
    }

    @Transactional
    public Optional<User> findByUsername(String username) {
        return userRepository.findFirstByName(username);
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findFirstByName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with name: " + username));


        List<GrantedAuthority> roles = new ArrayList<>();
        roles.add(new SimpleGrantedAuthority(user.getRole().name()));

        return new org.springframework.security.core.userdetails.User(
                user.getName(),
                user.getPassword(),
                roles
        );
    }
}
