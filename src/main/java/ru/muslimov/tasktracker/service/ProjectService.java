package ru.muslimov.tasktracker.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.muslimov.tasktracker.dto.ProjectDto;
import ru.muslimov.tasktracker.exceptions.models.BadRequestException;
import ru.muslimov.tasktracker.exceptions.models.NotFoundException;
import ru.muslimov.tasktracker.mapper.ProjectMapper;
import ru.muslimov.tasktracker.models.Project;
import ru.muslimov.tasktracker.repository.ProjectRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    @Transactional
    public ProjectDto createProject(String projectName) {
        projectRepository.findByName(projectName)
                .ifPresent(project -> {
                    throw new BadRequestException(String.format("Project %s already exist.", projectName));
                });

        Project project = projectRepository.saveAndFlush(
                Project.builder()
                        .name(projectName)
                        .build()
        );

        return projectMapper.toDto(project);
    }

    @Transactional
    public ProjectDto editProject(Long projectId, String projectName) {
        if (projectName.isBlank()) {
            throw new BadRequestException("Name can't be empty.");
        }

        Project project = getProjectOrThrowException(projectId);

        project.setName(projectName);

        project = projectRepository.saveAndFlush(project);

        return projectMapper.toDto(project);
    }

    @Transactional
    public ProjectDto getProject(Long projectId) {

        Project project = getProjectOrThrowException(projectId);

        return projectMapper.toDto(project);
    }

    @Transactional
    public List<ProjectDto> getProjects() {

        final List<Project> projects = projectRepository.findAll();

        return projects.stream().map(projectMapper::toDto).toList();
    }

    @Transactional
    public Boolean deleteProject(Long projectId) {

        getProjectOrThrowException(projectId);

        projectRepository.deleteById(projectId);

        return true;
    }

    protected Project getProjectOrThrowException(Long projectId) {
        return projectRepository.findById(projectId)
                .orElseThrow(() ->
                        new NotFoundException(String.format("Project with id:%s doesn't exist.", projectId)));
    }
}
