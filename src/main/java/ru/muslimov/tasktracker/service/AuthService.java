package ru.muslimov.tasktracker.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import ru.muslimov.tasktracker.dto.CreationUserDto;
import ru.muslimov.tasktracker.dto.JwtRequest;
import ru.muslimov.tasktracker.dto.JwtResponse;
import ru.muslimov.tasktracker.dto.UserDto;
import ru.muslimov.tasktracker.exceptions.models.BadRequestException;
import ru.muslimov.tasktracker.utils.JwtTokenUtils;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserService userService;
    private final JwtTokenUtils jwtTokenUtils;
    private final AuthenticationManager authenticationManager;

    public ResponseEntity<?> createAuthToken(@RequestBody JwtRequest authRequest) {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new BadRequestException("Incorrect login or password");
        }
        UserDetails userDetails = userService.loadUserByUsername(authRequest.getUsername());
        String token = jwtTokenUtils.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    public ResponseEntity<?> createUser(@RequestBody CreationUserDto creationUserDto) {

        if (!creationUserDto.getPassword().equals(creationUserDto.getConfirmPassword())) {
            throw new BadRequestException("The passwords don't match");
        }

        userService.findByUsername(creationUserDto.getUsername()).ifPresent(it -> {
            throw new BadRequestException("A user with the specified name already exists");
        });

        UserDto newUser = userService.createUser(creationUserDto);

        return ResponseEntity.ok(newUser);
    }
}
