package ru.muslimov.tasktracker.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.muslimov.tasktracker.dto.TaskStateDto;
import ru.muslimov.tasktracker.models.TaskState;

@RequiredArgsConstructor
@Component
public class TaskStateMapper {

    private final TaskMapper taskMapper;

    public TaskStateDto toDto(TaskState entity) {

        return TaskStateDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .tasks(entity
                        .getTasks()
                        .stream()
                        .map(taskMapper::toDto)
                        .toList())
                .build();
    }
}
