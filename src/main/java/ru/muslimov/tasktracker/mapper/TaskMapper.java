package ru.muslimov.tasktracker.mapper;

import org.springframework.stereotype.Component;
import ru.muslimov.tasktracker.dto.TaskDto;
import ru.muslimov.tasktracker.models.Task;

@Component
public class TaskMapper {

    public TaskDto toDto(Task entity) {

        return TaskDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .createAt(entity.getCreateAt())
                .description(entity.getDescription())
                .build();
    }
}
