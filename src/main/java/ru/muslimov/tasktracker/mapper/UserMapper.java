package ru.muslimov.tasktracker.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.muslimov.tasktracker.dto.UserDto;
import ru.muslimov.tasktracker.models.User;

@RequiredArgsConstructor
@Component
public class UserMapper {

    private final ProjectMapper projectMapper;

    public UserDto toDto(User entity) {

        return UserDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .email(entity.getEmail())
                .projects(entity
                        .getProjects()
                        .stream()
                        .map(projectMapper::toDto)
                        .toList()
                )
                .build();
    }

}
