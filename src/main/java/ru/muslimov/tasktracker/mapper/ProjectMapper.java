package ru.muslimov.tasktracker.mapper;

import org.springframework.stereotype.Component;
import ru.muslimov.tasktracker.dto.ProjectDto;
import ru.muslimov.tasktracker.models.Project;

@Component
public class ProjectMapper {

    public ProjectDto toDto(Project entity) {

        return ProjectDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .createAt(entity.getCreateAt())
                .build();
    }
}
