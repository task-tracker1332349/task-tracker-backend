package ru.muslimov.tasktracker.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.muslimov.tasktracker.dto.ProjectDto;
import ru.muslimov.tasktracker.service.ProjectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/projects")
public class ProjectController {

    private final ProjectService projectService;

    @PostMapping()
    public HttpEntity<ProjectDto> createTable(@RequestParam String projectName) {

        ProjectDto createdProject = projectService.createProject(projectName);

        return ResponseEntity.ok(createdProject);
    }

    @GetMapping()
    public HttpEntity<List<ProjectDto>> getProjects() {

        List<ProjectDto> projects = projectService.getProjects();

        return ResponseEntity.ok(projects);
    }

    @GetMapping("/{project_id}")
    public HttpEntity<ProjectDto> getProject(@PathVariable("project_id") Long projectId) {

        ProjectDto project = projectService.getProject(projectId);

        return ResponseEntity.ok(project);
    }

    @PatchMapping("/{project_id}")
    public HttpEntity<ProjectDto> editProject(@PathVariable("project_id") Long projectId,
                                              @RequestParam String projectName) {

        ProjectDto editProjectprojectDto = projectService.editProject(projectId, projectName);

        return ResponseEntity.ok(editProjectprojectDto);
    }

    @DeleteMapping("/{project_id}")
    public HttpEntity<?> deleteProject(@PathVariable("project_id") Long projectId) {

        projectService.deleteProject(projectId);

        return ResponseEntity.noContent().build();
    }
}
