package ru.muslimov.tasktracker.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.muslimov.tasktracker.dto.TaskDto;
import ru.muslimov.tasktracker.service.TaskService;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/")
public class TaskController {

    private final TaskService taskService;

    private static final String CREATE_TASK = "task_states/{task_state_id}/tasks";
    private static final String GET_TASKS = "task_states/{task_state_id}/tasks";
    private static final String GET_TASK = "tasks/{task_id}";
    private static final String EDIT_TASK = "tasks/{task_id}";
    private static final String DELETE_TASK = "tasks/{task_id}";

    @PostMapping(CREATE_TASK)
    public HttpEntity<TaskDto> createTask(@PathVariable("task_state_id") Long taskStateId,
                                          @RequestParam() String taskName) {

        TaskDto createdTask = taskService.createTask(taskStateId, taskName);

        return ResponseEntity.ok(createdTask);
    }

    @GetMapping(GET_TASKS)
    public HttpEntity<List<TaskDto>> getTasks(@PathVariable("task_state_id") Long taskStateId) {
        List<TaskDto> tasks = taskService.getTasks(taskStateId);

        return ResponseEntity.ok(tasks);
    }

    @GetMapping(GET_TASK)
    public HttpEntity<TaskDto> getTask(@PathVariable("task_id") Long taskId) {

        TaskDto task = taskService.getTask(taskId);

        return ResponseEntity.ok(task);
    }

    @PatchMapping(EDIT_TASK)
    public HttpEntity<TaskDto> editTask(@PathVariable("task_id") Long taskId,
                                        @RequestParam(required = false) Optional<String> taskName,
                                        @RequestParam(required = false) Optional<String> taskDescription) {

        TaskDto editedTask = taskService.editTask(taskId, taskName, taskDescription);

        return ResponseEntity.ok(editedTask);
    }

    @DeleteMapping(DELETE_TASK)
    public HttpEntity<?> deleteTask(@PathVariable("task_id") Long taskId) {

        taskService.deleteTask(taskId);

        return ResponseEntity.noContent().build();
    }
}
