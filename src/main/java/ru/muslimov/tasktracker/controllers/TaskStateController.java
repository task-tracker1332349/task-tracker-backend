package ru.muslimov.tasktracker.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.muslimov.tasktracker.dto.TaskStateDto;
import ru.muslimov.tasktracker.service.TaskStateService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/")
public class TaskStateController {

    private final TaskStateService taskStateService;

    private static final String CREATE_TASK_STATE = "projects/{project_id}/task_states";
    private static final String GET_TASK_STATES = "projects/{project_id}/task_states";
    private static final String GET_TASK_STATE = "task_states/{task_state_id}";
    private static final String PUT_TASK_STATE = "task_states/{task_state_id}";
    private static final String DELETE_TASK_STATE = "task_states/{task_state_id}";

    @PostMapping(CREATE_TASK_STATE)
    public HttpEntity<TaskStateDto> createTaskState(@PathVariable("project_id") Long projectId,
                                                    @RequestParam() String taskStateName) {

        TaskStateDto createdTaskState = taskStateService.createTaskState(projectId, taskStateName);

        return ResponseEntity.ok(createdTaskState);
    }

    @GetMapping(GET_TASK_STATES)
    public HttpEntity<List<TaskStateDto>> getTaskStates(@PathVariable("project_id") Long projectId) {

        List<TaskStateDto> taskStates = taskStateService.getTaskStates(projectId);

        return ResponseEntity.ok(taskStates);
    }

    @GetMapping(GET_TASK_STATE)
    public HttpEntity<TaskStateDto> getTaskState(@PathVariable("task_state_id") Long taskStateId) {

        TaskStateDto taskState = taskStateService.getTaskState(taskStateId);

        return ResponseEntity.ok(taskState);
    }

    @PutMapping(PUT_TASK_STATE)
    public HttpEntity<TaskStateDto> updateTaskState(@PathVariable("task_state_id") Long taskStateId,
                                                    @RequestParam() String taskStateName) {

        TaskStateDto updatedTaskState = taskStateService.updateTaskState(taskStateId, taskStateName);

        return ResponseEntity.ok(updatedTaskState);
    }

    @DeleteMapping(DELETE_TASK_STATE)
    public HttpEntity<?> deleteTaskState(@PathVariable("task_state_id") Long taskStateId) {

        taskStateService.deleteTaskState(taskStateId);

        return ResponseEntity.noContent().build();
    }

}
