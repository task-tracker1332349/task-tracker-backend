package ru.muslimov.tasktracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.muslimov.tasktracker.models.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
