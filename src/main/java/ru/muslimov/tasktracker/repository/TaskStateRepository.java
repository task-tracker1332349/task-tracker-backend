package ru.muslimov.tasktracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.muslimov.tasktracker.models.TaskState;

@Repository
public interface TaskStateRepository extends JpaRepository<TaskState, Long> {

}
