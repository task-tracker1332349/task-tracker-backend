package ru.muslimov.tasktracker.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskState {

    private static final String SEQ_NAME = "task_state_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_NAME)
    @SequenceGenerator(name = SEQ_NAME, sequenceName = SEQ_NAME, allocationSize = 1)
    private Long id;

    private String name;

    @ManyToOne
    Project project;

    @Builder.Default
    @OneToMany()
    @JoinColumn(name = "task_state_id")
    private List<Task> tasks = new ArrayList<>();
}
