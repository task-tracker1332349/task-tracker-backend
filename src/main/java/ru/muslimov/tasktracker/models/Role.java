package ru.muslimov.tasktracker.models;

public enum Role {
    USER, ADMIN
}