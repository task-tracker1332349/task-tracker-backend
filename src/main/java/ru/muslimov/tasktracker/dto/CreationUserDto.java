package ru.muslimov.tasktracker.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreationUserDto {
    private String username;
    private String password;
    private String confirmPassword;
    private String email;
}
